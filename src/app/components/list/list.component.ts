import { Component, OnInit } from '@angular/core';
import {ArticleServiceService} from "../../services/article-service.service";
import {Article} from "../../interfaces/article";

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  articleList: Article[] = [];
  isLoading = true;

  constructor(private articleService: ArticleServiceService) { }

  ngOnInit(): void {
    this.articleService.getAllArticles().subscribe(data => {
      this.articleList = data;
      this.isLoading = false
    })
  }

}
