import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {ArticleServiceService} from "../../services/article-service.service";
import {Article} from "../../interfaces/article";

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {

  article?: Article;


  constructor(private activatedRoute: ActivatedRoute, private articleService: ArticleServiceService) { }

  ngOnInit(): void {
    let id = Number(this.activatedRoute.snapshot.paramMap.get('id'));

    this.articleService.getOneArticle(+id).subscribe(data => {
      this.article = data;
    })

  }

}
