import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import {Article} from "../interfaces/article";

@Injectable({
  providedIn: 'root'
})
export class ArticleServiceService {


  private apiUrl = 'https://localhost:8000/api/articles';
  constructor(private http: HttpClient) { }

  getAllArticles(): Observable<Article[]> {
    return this.http.get<Article[]>(this.apiUrl + '.json' );
  }

  getOneArticle(id?: number): Observable<Article> {
    return this.http.get<Article>(this.apiUrl + '/' + id + '.json');
  }
}
